package com.tugaskelompok.nabung;

public class KalkulatorController {
    private double jumlahInvestasi;
    private double bunga;
    private int durasi;

    public KalkulatorController(double jumlahInvestasi, double bunga, int durasi) {
        this.jumlahInvestasi = jumlahInvestasi;
        this.bunga = bunga;
        this.durasi = durasi;
    }

    public double hitungInvestasiSatuWaktu() {
        double bungaPerTahun = jumlahInvestasi * (bunga / 100);
        double jumlahBunga = bungaPerTahun * durasi / 12;
        return jumlahInvestasi + jumlahBunga;
    }

    public double hitungInvestasiBerkala() {
        double bungaBulanan = bunga / 100 / 12;
        return jumlahInvestasi * (Math.pow(1 + bungaBulanan, durasi) - 1) / bungaBulanan;
    }
}
