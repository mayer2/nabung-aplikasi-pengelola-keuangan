package com.tugaskelompok.nabung;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private Session session;

    private boolean isTwoPane;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        session = new Session(this);
        if (!session.loggedIn()){
            logout();
        }
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if (id == R.id.navigation_home){
                    showHalamanWishlist();
                }

                else if (id == R.id.navigation_pemasukan){
                    showHalamanCatatanPemasukan();
                }

                else if (id == R.id.navigation_pengeluaran){
                    showHalamanCatatanPengeluaran();
                }

                else if (id == R.id.navigation_kalkulator){
                    showHalamanKalkulator();
                }

                else if (id == R.id.navigation_profile){
                    showHalamanProfile();
                }
                return true;
            }
        });

        bottomNavigationView.setSelectedItemId(R.id.navigation_home);


    }
    private void logout(){
        session.setLoggedin(false);
        finish();
        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }

    private void showHalamanWishlist(){
        HomeFragment fragment = new HomeFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
        setTitle("List Keinginan");
    }
    private void showHalamanCatatanPemasukan(){
        PemasukanFragment fragment = new PemasukanFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
        setTitle("Pemasukan");
    }
    private void showHalamanCatatanPengeluaran(){
        PengeluaranFragment fragment = new PengeluaranFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
        setTitle("Pengeluaran");
    }
    private void showHalamanKalkulator(){
        KalkulatorFragment fragment = new KalkulatorFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
        setTitle("Kalkulator");
    }
    private void showHalamanProfile(){
        ProfileFragment fragment = new ProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.commit();
        setTitle("Profile");
    }
}