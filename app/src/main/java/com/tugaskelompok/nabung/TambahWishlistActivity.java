package com.tugaskelompok.nabung;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TambahWishlistActivity extends AppCompatActivity {
    private EditText mTambahJudul, mTambahSaldo, mTargetHari;
    private Button mTambahWishlist;
    private DatabaseReference mDatabaseRef;
    private String mUserID, mJum;
    private TextView tv_kembali;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_wishlist);
        mTambahJudul = findViewById(R.id.et_tambah_judul_wsh);
        mTambahSaldo = findViewById(R.id.et_tambah_saldo_wsh);
        mTargetHari = findViewById(R.id.et_tambah_hari_wsh);
        mTambahWishlist = findViewById(R.id.bt_tambah_wishlist);
        tv_kembali = findViewById(R.id.tv_kembali);

        tv_kembali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(TambahWishlistActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        mTambahSaldo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTambahSaldo.removeTextChangedListener(this);
                try {
                    String originalString = s.toString();
                    originalString = originalString.replaceAll("\\.", "").replaceFirst(",", ".");
                    originalString = originalString.replaceAll("[A-Z]", "").replaceAll("[a-z]", "");
                    int doubleval = Integer.parseInt(originalString);
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setDecimalSeparator(',');
                    symbols.setGroupingSeparator('.');
                    String pattern = "#,###,###";
                    DecimalFormat formatter = new DecimalFormat(pattern, symbols);
                    String formattedString = formatter.format(doubleval);
                    mJum = formattedString.replace(".","");
                    mTambahSaldo.setText(formattedString);
                    mTambahSaldo.setSelection(mTambahSaldo.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }
                mTambahSaldo.addTextChangedListener(this);


                mTambahSaldo.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            mUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();
        } else {
            startActivity(new Intent(TambahWishlistActivity.this, LoginActivity.class));
        }
        mDatabaseRef = FirebaseDatabase.getInstance().getReference(mUserID).child("wishlist");

        mTambahWishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String judul = mTambahJudul.getText().toString().trim();
                String saldo = mTambahSaldo.getText().toString().trim();
                String hari = mTargetHari.getText().toString().trim();
                if (TextUtils.isEmpty(judul)){
                    mTambahJudul.setError("Semua kolom harus terisi");
                } else if (TextUtils.isEmpty(saldo)) {
                    mTambahSaldo.setError("Semua kolom harus terisi");
                } else if (TextUtils.isEmpty(hari)) {
                    mTargetHari.setError("Semua kolom harus terisi");
                } else {
                    Calendar c1 = Calendar.getInstance();
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                    String tanggal = sdf1.format(c1.getTime());
                    String dataID = mDatabaseRef.push().getKey();
                    Wishlist upload = new Wishlist(dataID,judul, hari, mJum,"0", mJum, tanggal);

                    mDatabaseRef.child(dataID).setValue(upload);
                    finish();
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
