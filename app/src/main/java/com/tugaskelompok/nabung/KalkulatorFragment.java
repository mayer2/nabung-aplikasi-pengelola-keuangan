package com.tugaskelompok.nabung;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class KalkulatorFragment extends Fragment {
    private Session session;
    private EditText jumlahInvestasi, bunga, durasi;
    private RadioGroup radioGroup;
    private RadioButton rb_satuWaktu, rb_Berkala;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view_profil = inflater.inflate(R.layout.fragment_kalkulator, container, false);
        session = new Session(getContext());

        radioGroup = view_profil.findViewById(R.id.radio_group);
        jumlahInvestasi = view_profil.findViewById(R.id.et_jumlahInvestasi);
        bunga = view_profil.findViewById(R.id.et_bunga);
        durasi = view_profil.findViewById(R.id.et_durasi);
        rb_satuWaktu = view_profil.findViewById(R.id.rb_satuwaktu);
        rb_Berkala = view_profil.findViewById(R.id.rb_berkala);

        Button btHitung = view_profil.findViewById(R.id.bt_Hitung);
        btHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hitungInvestasi();
            }
        });

        return view_profil;
    }

    private void hitungInvestasi() {
        String getJumlahInvestasi = jumlahInvestasi.getText().toString();
        String getBunga = bunga.getText().toString();
        String getDurasi = durasi.getText().toString();

        if (getJumlahInvestasi.isEmpty()) {
            jumlahInvestasi.setError("Semua kolom harus terisi");
        } else if (getBunga.isEmpty()) {
            bunga.setError("Semua kolom harus terisi");
        } else if (getDurasi.isEmpty()) {
            durasi.setError("Semua kolom harus terisi");
        } else {
            double jumlahInvestasiValue = Double.parseDouble(getJumlahInvestasi);
            double bungaValue = Double.parseDouble(getBunga);
            int durasiValue = Integer.parseInt(getDurasi);

            KalkulatorController controller = new KalkulatorController(jumlahInvestasiValue, bungaValue, durasiValue);

            int selectedRadioButtonId = radioGroup.getCheckedRadioButtonId();

            if (selectedRadioButtonId == R.id.rb_satuwaktu) {
                double hasilInvestasi = controller.hitungInvestasiSatuWaktu();
                tampilkanHasilInvestasi(hasilInvestasi, durasiValue);
            } else if (selectedRadioButtonId == R.id.rb_berkala) {
                double hasilInvestasi = controller.hitungInvestasiBerkala();
                tampilkanHasilInvestasi(hasilInvestasi, durasiValue);
            }
        }
    }

    private void tampilkanHasilInvestasi(double hasilInvestasi, int durasi) {
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(new Locale("id", "ID"));
        String hasilInvestasiFormatted = formatRupiah.format(hasilInvestasi);

        TextView tvProyeksi = getView().findViewById(R.id.tv_proyeksi);
        tvProyeksi.setText(hasilInvestasiFormatted);

        TextView tvProyeksiDurasi = getView().findViewById(R.id.tv_proyeksiDurasi);
        tvProyeksiDurasi.setText("Dalam " + durasi + " bulan");
    }
}

